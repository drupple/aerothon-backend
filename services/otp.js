const AM = require('./accountManager')
const Mailer = require('./mailer');

function generateOTP(length) {
    let possibleValues = '0123456789';
    let OTP = '';
    for(let i = 0; i < length; i++) {
        OTP += possibleValues[Math.floor(Math.random()*10)]
    }
    return OTP;
}

const sendOTP = (async (email) => {
    const newOTP = generateOTP(4);
    try {
        await AM.storeOTP(email, newOTP);
        Mailer({to: email, type: 'OTP', text: newOTP, subject: "Airbuzz OTP for Login"})
    } catch (e) {
        console.error('\nOTP : COULD NOT STORE & SEND OTP');
    }
})

const verifyOTP = (async (email, OTP) => {
    try {
        const storedOTP = await AM.getStoredOTP(email);
        if(storedOTP === OTP) {
            console.log("\nOTP : OTP VERIFIED for email : ", email);
            await AM.verifyAccount(email);
            return "OTP_VERIFIED";
        } else {
            console.log("\nOTP : INCORRECT OTP for email : ", email);
            return "OTP_NOT_CORRECT";
        }
    } catch (e) {
        console.error('\nOTP : COUNT NOT FETCH STORED OTP');
        throw "OTP_NOT_FETCHED";
    }
})



module.exports = {
    sendOTP,
    verifyOTP,
    generateOTP
}