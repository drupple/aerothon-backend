/**
 * DB Operations for Airbuzz
 */

const MongoClient = require('mongodb').MongoClient;

const DB_URL = 'mongodb://localhost:27017';
const DB_NAME = 'airbus';

// create group
// add members
// if one member adds the mandate, the group members get the same request


const register = (({email}) => {
    return new Promise((resolve, reject) => {
        MongoClient.connect(DB_URL, {useNewUrlParser: true}, async (err, client) => {
            
            if(err) {
                reject('NO_DB_CONNECTION');
            } else {
                const DB = client.db(DB_NAME);
                const profile = DB.collection('profile');
                
                const alreadyExists = await profile.findOne({email});
                
                if(alreadyExists) {
                    console.log("\nAM : EMAIL ALREADY EXISTS : ", email);
                    reject('EMAIL_ALREADY_EXISTS');
                } else {
                    try {
                        const insertResult = await profile.insertOne({email, otpVerified: false});
                        console.log("\nAM : REGISTRATION SUCCESSFUL OF : ", email);
                        resolve('REGISTRATION_SUCCESSFUL');
                    } catch (e) {
                        console.error("\nAM : REGISTRATION FAILED :-");
                        console.error(e);
                        reject('REGISTRATION_FAILED');
                    }
                }
            }
            
        })
    })
})

const storeOTP = ((email, otp) => {
    return new Promise((resolve, reject) => {
        MongoClient.connect(DB_URL, {useNewUrlParser: true}, async (err, client) => {
            if(err) {
                reject('NO_DB_CONNECTION');
            } else {
                const DB = client.db(DB_NAME);
                const profile = DB.collection('profile');
                
                try {
                    const updateResult = await profile.updateOne({email}, {$set : {otp: otp, otpVerified: false}}, {upsert: false});
                    if(updateResult.result.n) {
                        console.log("\nAM : OTP GENERATED AND STORED FOR : ", email);
                        resolve("OTP_GENERATED");
                    } else {
                        console.error("\nAM : OTP UPDATION FAILED FOR : ", email);
                        reject('OTP_UPDATION_FAILED');
                    }
                } catch (e) {
                    console.error(e);
                    reject('OTP_GENERATION_FAILED');
                }
            }
        })
    })
})

const getStoredOTP = ((email) => {
    return new Promise((resolve, reject) => {
        MongoClient.connect(DB_URL, {useNewUrlParser: true}, async (err, client) => {
            if(err) {
                reject('NO_DB_CONNECTION');
            } else {
                const DB = client.db(DB_NAME);
                const profile = DB.collection('profile');
                
                try {
                    const storedOTP = await profile.findOne({email}, {projection: {otp: 1, _id: 0}});
                    if(storedOTP) {
                        resolve(storedOTP.otp);
                    } else {
                        reject('NO_OTP_FOUND');
                    }
                } catch (e) {
                    console.error(e);
                    reject('ERROR_OTP_FETCH');
                }

            }
        })
    })
})

const verifyAccount = ((email) => {
    return new Promise((resolve, reject) => {
        MongoClient.connect(DB_URL, {useNewUrlParser: true}, async (err, client) => {
            if(err) {
                reject('NO_DB_CONNECTION');
            } else {
                const DB = client.db(DB_NAME);
                const profile = DB.collection('profile');
                
                try {
                    await profile.updateOne({email}, {$set: {otpVerified: true}});
                    console.log("\nAM : OTP STATUS SUCCESS SET for email : ", email);
                    resolve('OTP_VERIFIED_SET_SUCCESS');
                } catch (e) {
                    console.error(e);
                    reject('OTP_VERIFIED_SET_FAILED');
                }
            }
        })
    })
})

const addFlightData = ((flightData) => {
    return new Promise((resolve, reject) => {
        MongoClient.connect(DB_URL, {useNewUrlParser: true}, async (err, client) => {
            if(err) {
                reject('NO_DB_CONNECTION');
            } else {
                const DB = client.db(DB_NAME);
                const flights = DB.collection('flights');
                
                try {
                    const flightInsert = await flights.insertOne(flightData);
                    console.log("\nAM : FLIGHT DATA INSERT SUCCESSFUL OF FLIGHT: ", flightData.MSN);
                    resolve('FLIGHT DATA INSERT_SUCCESSFUL');
                } catch (e) {
                    console.error("\nAM : FLIGHT DATA INSERT FAILED :-");
                    console.error(e);
                    reject('FLIGHT DATA INSERT_FAILED');
                }
            }
        })
    })
})

const getNewsData = (() => {
    return new Promise((resolve, reject) => {
        MongoClient.connect(DB_URL, {useNewUrlParser: true}, async (err, client) => {
            if(err) {
                reject('NO_DB_CONNECTION');
            } else {
                const DB = client.db(DB_NAME);
                const news = DB.collection('news');
                
                try {
                    const newsData = await news.find({});
                    if(newsData) {
                        newsData.toArray((err, result) => {
                            console.log(result);
                            resolve(result);
                        });
                    } else {
                        reject('NO_NEWS_FOUND');
                    }
                } catch (e) {
                    console.error(e);
                    reject('ERROR_NEWS_FETCH');
                }

            }
        })
    })
})

const getFlights = (() => {
    return new Promise((resolve, reject) => {
        MongoClient.connect(DB_URL, {useNewUrlParser: true}, async (err, client) => {
            if(err) {
                reject('NO_DB_CONNECTION');
            } else {
                const DB = client.db(DB_NAME);
                const flights = DB.collection('flights');
                
                try {
                    const flightsData = await flights.find({});
                    if(flightsData) {
                        flightsData.toArray((err, result) => {
                            console.log(result);
                            resolve(result);
                        });
                    } else {
                        reject('NO_NEWS_FOUND');
                    }
                } catch (e) {
                    console.error(e);
                    reject('ERROR_NEWS_FETCH');
                }

                // const aggregatedData = await flights.aggregate({'$group' : {'_id' : "$MSN", 'arr': {'$push': "$$ROOT"}}});
                // console.log(aggregatedData);
                // aggregatedData.toArray((err, result) => {
                    
                //     var ageNodes = result.reduce(function(obj, doc) { 
                //         obj[doc.MSN] = [];
                //         return obj;
                //     }, {});

                //     result.forEach((item) => {
                //         ageNodes[item.MSN].push(item)
                //     })
                //     console.log(ageNodes);
                //     resolve(ageNodes);

                // })

            }
        })
    })
})

const setReminderFlight = ((email, flightNo) => {
    return new Promise((resolve, reject) => {
        MongoClient.connect(DB_URL, {useNewUrlParser: true}, async (err, client) => {
            if(err) {
                reject('NO_DB_CONNECTION');
            } else {
                const DB = client.db(DB_NAME);
                const profile = DB.collection('profile');
                
                try {
                    const updateResult = await profile.updateOne({email}, {$set : {flightNo, status: 'NOT_ARRIVED'}}, {upsert: false});
                    if(updateResult.result.n) {
                        console.log("\nAM : REMINDER REMINDER SET AND STORED FOR : ", email);
                        resolve("REMINDER_GENERATED");
                    } else {
                        console.error("\nAM : REMINDER UPDATION FAILED FOR : ", email);
                        reject('REMINDER_UPDATION_FAILED');
                    }
                } catch (e) {
                    console.error(e);
                    reject('REMINDER_GENERATION_FAILED');
                }
            }
        })
    })
})

const updateFlightStatus = ((flightNo) => {
    return new Promise((resolve, reject) => {
        MongoClient.connect(DB_URL, {useNewUrlParser: true}, async (err, client) => {
            if(err) {
                reject('NO_DB_CONNECTION');
            } else {
                const DB = client.db(DB_NAME);
                const profile = DB.collection('profile');
                
                try {
                    const updateResult = await profile.findOneAndUpdate({flightNo}, {$set : {status: 'ARRIVED'}}, {upsert: false});
                    if(updateResult) {
                        console.log(updateResult.value.email);
                        resolve(updateResult.value.email);
                    } else {
                        console.error("\nAM : REMINDER UPDATION FAILED FOR : ", email);
                        reject('REMINDER_UPDATION_FAILED');
                    }
                } catch (e) {
                    console.error(e);
                    reject('REMINDER_GENERATION_FAILED');
                }
            }
        })
    })
})

const getDataByKey = ((key, value) => {
    return new Promise((resolve, reject) => {
        MongoClient.connect(DB_URL, {useNewUrlParser: true}, async (err, client) => {
            if(err) {
                reject('NO_DB_CONNECTION');
            } else {
                const DB = client.db(DB_NAME);
                const flights = DB.collection('flights');
                
                try {
                    const flightsList = await flights.find({[key]: value});
                    if(flightsList) {
                        flightsList.toArray((err, result) => {
                            console.log(result);
                            resolve(result);
                        })
                    } else {
                        reject('NO_FLIGHTS_FOUND');
                    }
                } catch (e) {
                    console.error(e);
                    reject('ERROR_FLIGHTS_FETCH');
                }

            }
        })
    })
})

const getRangeDataByKey = ((key, low, high) => {
    return new Promise((resolve, reject) => {
        MongoClient.connect(DB_URL, {useNewUrlParser: true}, async (err, client) => {
            if(err) {
                reject('NO_DB_CONNECTION');
            } else {
                const DB = client.db(DB_NAME);
                const flights = DB.collection('flights');
                
                try {
                    const flightsList = await flights.find({[key]: {$gte : low, $lte: high}});
                    if(flightsList) {
                        flightsList.toArray((err, result) => {
                            console.log(result);
                            resolve(result);
                        })
                    } else {
                        reject('NO_FLIGHTS_FOUND');
                    }
                } catch (e) {
                    console.error(e);
                    reject('ERROR_FLIGHTS_FETCH');
                }

            }
        })
    })
})

const getDataByKeys = ((keys) => {
    return new Promise((resolve, reject) => {
        MongoClient.connect(DB_URL, {useNewUrlParser: true}, async (err, client) => {
            if(err) {
                reject('NO_DB_CONNECTION');
            } else {
                const DB = client.db(DB_NAME);
                const flights = DB.collection('flights');
                console.log(keys);
                
                try {
                    const flightsList = await flights.find({$and : keys});
                    if(flightsList) {
                        flightsList.toArray((err, result) => {
                            console.log(result);
                            resolve(result);
                        })
                    } else {
                        reject('NO_FLIGHTS_FOUND');
                    }
                } catch (e) {
                    console.error(e);
                    reject('ERROR_FLIGHTS_FETCH');
                }

            }
        })
    })
})


module.exports = {
    register,
    storeOTP,
    getStoredOTP,
    verifyAccount,
    getNewsData,
    addFlightData,
    getFlights,
    setReminderFlight,
    updateFlightStatus,
    getRangeDataByKey,
    getDataByKey,
    getDataByKeys
}