/**
 * Nodemailer module for Airbuzz.
 * sends mail depending on the type of mail.
 * OTP, Registration, etc.
 */

const path = require('path');
const nodemailer = require('nodemailer');
const _FROM = 'groupees2019@gmail.com'
const _PASS = process.env.G_PASS;

const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: _FROM,
        pass: _PASS
    }
})

const getEmailTemplate = (type, text) => {
    if(type == 'OTP') {
        let HTML = `<html> <head> <title>Airbuzz</title> <style> .container { position: absolute; top: 0%; left: 0%; margin: 0; width: 100%; height: auto; padding: 0; font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen", "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue", sans-serif; -webkit-font-smoothing: antialiased; -moz-osx-font-smoothing: grayscale; } .topSection { display: flex; flex-direction: row; align-items: center; justify-content: center; width: 100%; margin-top: 1.5rem; } .textSection { display: flex; flex-direction: row; align-items: center; justify-content: center; width: 100%; margin-top: 1.5rem; } .footerSection { display: flex; flex-direction: row; align-items: center; justify-content: center; width: 100%; margin-top: 1.5rem; } </style> </head> <body> <div class="container"> <div class="topSection"> <img style="height: 100px;" src="cid:airbus@node.com"/> </div> <div class="textSection"> Your OTP for regsitration : ${text} </div> <div class="footerSection"> Airbuzz, 2019 </div> </div> </body> </html>`
        return HTML;
    }

    if(type == 'REMINDER') {
        let HTML = `<html> <head> <title>Airbuzz</title> <style> .container { position: absolute; top: 0%; left: 0%; margin: 0; width: 100%; height: auto; padding: 0; font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen", "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue", sans-serif; -webkit-font-smoothing: antialiased; -moz-osx-font-smoothing: grayscale; } .topSection { display: flex; flex-direction: row; align-items: center; justify-content: center; width: 100%; margin-top: 1.5rem; } .textSection { display: flex; flex-direction: row; align-items: center; justify-content: center; width: 100%; margin-top: 1.5rem; } .footerSection { display: flex; flex-direction: row; align-items: center; justify-content: center; width: 100%; margin-top: 1.5rem; } </style> </head> <body> <div class="container"> <div class="topSection"> <img style="height: 100px;" src="cid:airbus@node.com"/> </div> <div class="textSection"> Flight Reminder : ${text} Arrived </div> <div class="footerSection"> Airbuzz, 2019 </div> </div> </body> </html>`
        return HTML;
    }
}

const sendEmail = ({to, subject = 'Airbuzz, For You!', text, type}) => {
    if(!_PASS) {
        console.error("\nMAILER : PASSWORD NOT SET. EMAIL NOT SENT.");
    } else {
        console.log("\nMAILER : Initializing Mail to : ", to);
        let emailTemplate = getEmailTemplate(type, text);
        let mailOptions = {
            to,
            subject,
            from: _FROM,
            html: emailTemplate,
            attachments: [{
                filename: 'airbus.png',
                path: path.resolve(__dirname, '../assets', 'airbus.png'),
                cid: 'airbus@node.com' //same cid value as in the html img src
            }]
        }
        transporter.sendMail(mailOptions)
        .then((info) => {
            console.log(`\nMAILER : Mail Status to ${to} : `, info.response);
        })
        .catch((err) => {
            console.error(`\nMAILER : Error in sending mail to ${to}:-`);
            console.error(err);
        })
    }
}

module.exports = sendEmail;





