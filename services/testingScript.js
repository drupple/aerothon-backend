/**
 * DB Operations for Groupees
 */

const MongoClient = require('mongodb').MongoClient;

const DB_URL = 'mongodb://localhost:27017';
const DB_NAME = 'airbus';

// create group
// add members
// if one member adds the mandate, the group members get the same request


const register = (({email}) => {
    return new Promise((resolve, reject) => {
        MongoClient.connect(DB_URL, {useNewUrlParser: true}, async (err, client) => {
            
            if(err) {
                reject('NO_DB_CONNECTION');
            } else {
                const DB = client.db(DB_NAME);
                const profile = DB.collection('profile');
                
                const alreadyExists = await profile.findOne({email});
                
                if(alreadyExists) {
                    console.log("\nAM : EMAIL ALREADY EXISTS : ", email);
                    reject('EMAIL_ALREADY_EXISTS');
                } else {
                    try {
                        const insertResult = await profile.insertOne({email, otpVerified: false});
                        console.log("\nAM : REGISTRATION SUCCESSFUL OF : ", email);
                        resolve('REGISTRATION_SUCCESSFUL');
                    } catch (e) {
                        console.error("\nAM : REGISTRATION FAILED :-");
                        console.error(e);
                        reject('REGISTRATION_FAILED');
                    }
                }
            }
            
        })
    })
})

const storeOTP = ((email, otp) => {
    return new Promise((resolve, reject) => {
        MongoClient.connect(DB_URL, {useNewUrlParser: true}, async (err, client) => {
            if(err) {
                reject('NO_DB_CONNECTION');
            } else {
                const DB = client.db(DB_NAME);
                const profile = DB.collection('profile');
                
                try {
                    const updateResult = await profile.updateOne({email}, {$set : {otp: otp}}, {upsert: false});
                    if(updateResult.result.n) {
                        console.log("\nAM : OTP GENERATED AND STORED FOR : ", email);
                        resolve("OTP_GENERATED");
                    } else {
                        console.error("\nAM : OTP UPDATION FAILED FOR : ", email);
                        reject('OTP_UPDATION_FAILED');
                    }
                } catch (e) {
                    console.error(e);
                    reject('OTP_GENERATION_FAILED');
                }
            }
        })
    })
})

const getStoredOTP = ((email) => {
    return new Promise((resolve, reject) => {
        MongoClient.connect(DB_URL, {useNewUrlParser: true}, async (err, client) => {
            if(err) {
                reject('NO_DB_CONNECTION');
            } else {
                const DB = client.db(DB_NAME);
                const profile = DB.collection('profile');
                
                try {
                    const storedOTP = await profile.findOne({email}, {projection: {otp: 1, _id: 0}});
                    if(storedOTP) {
                        resolve(storedOTP.otp);
                    } else {
                        reject('NO_OTP_FOUND');
                    }
                } catch (e) {
                    console.error(e);
                    reject('ERROR_OTP_FETCH');
                }

            }
        })
    })
})

const verifyAccount = ((email) => {
    return new Promise((resolve, reject) => {
        MongoClient.connect(DB_URL, {useNewUrlParser: true}, async (err, client) => {
            if(err) {
                reject('NO_DB_CONNECTION');
            } else {
                const DB = client.db(DB_NAME);
                const profile = DB.collection('profile');
                
                try {
                    await profile.updateOne({email}, {$set: {otpVerified: true}});
                    console.log("\nAM : OTP STATUS SUCCESS SET for email : ", email);
                    resolve('OTP_VERIFIED_SET_SUCCESS');
                } catch (e) {
                    console.error(e);
                    reject('OTP_VERIFIED_SET_FAILED');
                }
            }
        })
    })
})


const getNewsData = (() => {
    return new Promise((resolve, reject) => {
        MongoClient.connect(DB_URL, {useNewUrlParser: true}, async (err, client) => {
            if(err) {
                reject('NO_DB_CONNECTION');
            } else {
                const DB = client.db(DB_NAME);
                const news = DB.collection('news');
                
                try {
                    const newsData = await news.findOne({});
                    if(newsData) {
                        console.log(newsData);
                        console.log(newsData.toArray());
                        resolve(newsData);
                    } else {
                        reject('NO_NEWS_FOUND');
                    }
                } catch (e) {
                    console.error(e);
                    reject('ERROR_NEWS_FETCH');
                }

            }
        })
    })
})

const newsArray = [
    {
        category: 'flight',
        time: new Date(),
        headline: 'Airbus',
        text: "It's Amazing..."
    },
    {
        category: 'car',
        time: new Date(),
        headline: 'Airbus',
        text: "It's Amazing..."
    },
    {
        category: 'ship',
        time: new Date(),
        headline: 'Airbus',
        text: "It's Amazing..."
    },
    {
        category: 'cycle',
        time: new Date(),
        headline: 'Airbus',
        text: "It's Amazing..."
    }
];

const insertNewsData = (() => {
    return new Promise((resolve, reject) => {
        MongoClient.connect(DB_URL, {useNewUrlParser: true}, async (err, client) => {
            if(err) {
                reject('NO_DB_CONNECTION');
            } else {
                const DB = client.db(DB_NAME);
                const news = DB.collection('news');
                
                try {
                    const newsData = await news.insertMany(newsArray);
                    if(newsData) {
                        console.log(newsData);
                        //console.log(newsData.toArray());
                        resolve(newsData);
                    } else {
                        reject('NO_NEWS_FOUND');
                    }
                } catch (e) {
                    console.error(e);
                    reject('ERROR_NEWS_FETCH');
                }

            }
        })
    })
})

insertNewsData()
.then((result) => {
    console.log(result);
})


