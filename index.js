const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const AM = require('./services/accountManager');
const OTP = require('./services/otp');
const moment = require('moment');
const Mailer = require('./services/mailer');

const _PORT = 3000;

const app = express();
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));


app.post('/register', async (req, res) => {
    const email = req.body.email;

    try {
        const registration = await AM.register({email});
        res.status(200).send({result: registration, error: null});
    } catch (e) {
        res.status(400).send({result: null, error: e});
    }

})

app.post('/login', async (req, res) => {
    const email = req.body.email;

    try {
        await OTP.sendOTP(email);
        res.status(200).send({result: 'OTP_SENT', error: null});
    } catch (e) {
        res.status(400).send({result: null, error: e});
    }

})

app.post('/verifyOTP', async (req, res) => {
    const receivedOTP = req.body.OTP;
    const email = req.body.email;

    try {
        const OTPstatus = await OTP.verifyOTP(email, receivedOTP);
        res.status(200).send({result: OTPstatus, error: null});
    } catch (e) {
        res.status(400).send({result: null, error: e});
    }
})

app.get('/getNews', async (req, res) => {

    try {
        const newsData = await AM.getNewsData();
        res.status(200).send({result: newsData});
    } catch (e) {
        res.status(200).send({result: []});
    }

})

app.post('/addFlight', async (req, res) => {

    console.log("POST /addFlight");

    const flightData = {
        vendor: req.body.vendor || 'Indigo',
        model: req.body.model || 'A320',
        MSN: +req.body.MSN || OTP.generateOTP(6),
        harnessLength: +req.body.harnessLength || 0,
        grossWeight: +req.body.grossWeight || 0,
        atPressure: +req.body.atPressure || 0,
        roomTemp: +req.body.roomTemp || 0,
        airport: req.body.airport || 'BLR',
        FCleftWing: +req.body.fcLeftWing || 0,
        FCrightWing: +req.body.fcRightWing || 0, 
        FQleftWing: +req.body.fqLeftWing || 0, 
        FQrightWing: +req.body.fqRightWing || 0,
        maxAlt: +req.body.maxAlt || 0,
        flightNo: req.body.flightNo || 'GE ' + OTP.generateOTP(3),
        time: moment.utc().format('HH:mm') 
    }

    try {
        const insertResult = await AM.addFlightData(flightData);
        res.status(200).send({result: insertResult, error: null});
    } catch (e) {
        res.status(200).send({result: null, error: e});
    }
})

app.get('/addFlight', async (req, res) => {

    console.log("GET /addFlight");

    const flightData = {
        vendor: req.body.vendor || 'Indigo',
        model: req.body.model || 'A320',
        MSN: +req.body.MSN || OTP.generateOTP(6),
        harnessLength: +req.body.harnessLength || 0,
        grossWeight: +req.body.grossWeight || 0,
        atPressure: +req.body.atPressure || 0,
        roomTemp: +req.body.roomTemp || 0,
        airport: req.body.airport || 'BLR',
        FCleftWing: +req.body.fcLeftWing || 0,
        FCrightWing: +req.body.fcRightWing || 0, 
        FQleftWing: +req.body.fqLeftWing || 0, 
        FQrightWing: +req.body.fqRightWing || 0,
        maxAlt: +req.body.maxAlt || 0,
        flightNo: req.body.flightNo || 'GE ' + OTP.generateOTP(3),
        time: req.body.time || moment.utc().format('HH:mm') 
    }

    res.status(200).send(flightData);
})

app.get('/getFlights', async (req, res) => {

    try {
        const flightsList = await AM.getFlights();
        res.status(200).send({result: flightsList, error: null});
    } catch (e) {
        res.status(200).send({result: null, error: e});
    }
})

app.post('/setReminder', async (req, res) => {
    const email = req.body.email;
    const flightNo = req.body.flightNo;

    try {
        const reminderStatus = await AM.setReminderFlight(email, flightNo);
        res.status(200).send({result: reminderStatus, error: null});
    } catch (e) {
        res.status(200).send({result: null, error: e});
    }
})

app.post('/flightStatusUpdate', async (req, res) => {
    const flightNo = req.body.flightNo || 'GE 123';

    try {
        const email = await AM.updateFlightStatus(flightNo);
        Mailer({to: email, subject: "Airbuzz Flight Reminder", text: flightNo, type: "REMINDER"});
        res.status(200).send({result: email, error: null});
    } catch (e) {
        res.status(200).send({result: null, error: e});
    }
})

app.post('/getFilteredData', async (req, res) => {
    const flightData = {
        vendor: req.body.vendor,
        model: req.body.model,
        MSN: +req.body.MSN,
        harnessLength: +req.body.harnessLength,
        grossWeight: +req.body.grossWeight,
        atPressure: +req.body.atPressure,
        roomTemp: +req.body.roomTemp,
        airport: req.body.airport,
        FCleftWing: +req.body.fcLeftWing,
        FCrightWing: +req.body.fcRightWing, 
        FQleftWing: +req.body.fqLeftWing, 
        FQrightWing: +req.body.fqRightWing,
        maxAlt: +req.body.maxAlt,
        flightNo: req.body.flightNo,
        time: req.body.time 
    }

    const keys = Object.keys(flightData);
    let finalObject = [];
    keys.forEach((item) => {
        if(flightData[item]) {
            finalObject.push({[item]: flightData[item]});
        }
    })

    try {
        const list = await AM.getDataByKeys(finalObject);
        res.status(200).send({result: list, error: null});
    } catch (e) {
        res.statys(200).send({result: null, error: e});
    }
    
})

app.post('/getRangeData', async (req, res) => {
    const searchKey = req.body.key || 'harnessLength';
    const low = +req.body.low || 0;
    const high = +req.body.high || 10;

    try {
        const searchResult = await AM.getRangeDataByKey(searchKey, low, high);
        res.status(200).send({result: searchResult, error: null});
    } catch (e) {
        res.status(200).send({result: null, error: e});
    }
})

app.get('/check', (req, res) => {
    res.status(200).send({result: 'success'});
})

app.listen(_PORT, () => {
    console.log(`\n## SERVER STARTED ON PORT : ${_PORT}\n`);
})

